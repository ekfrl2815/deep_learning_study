
class MulLayer:
    def __init__(self):
        self.x = None
        self.y = None

    def forward(self, x, y):
        self.x = x
        self.y = y
        out = x * y

        return out

    def backward(self, dout):
        dx = dout * self.y
        dy = dout * self.x

        return dx, dy

class AddLayer:
    def __init__(self):
        pass

    def forward(self, x, y):
        out = x + y
        return out

    def backward(self, dout):
        dx = dout * 1
        dy = dout * 1
        return dx, dy


apple = 100
apple_num = 2
orange = 150
orange_num = 3
tax = 1.1

# 계층 구현
mul_apple_layer = MulLayer()
mul_orange_layer = MulLayer()
add_apple_orange_layer = AddLayer()
mul_tax_layer = MulLayer()

# 순전파
apple_price = mul_apple_layer.forward(apple, apple_num) # 사과 x 사과갯수
orange_price = mul_orange_layer.forward(orange, orange_num) # 오렌지 X 오렌지갯수
apple_orange_price = add_apple_orange_layer.forward(apple_price, orange_price) # 사과금액 + 오렌지금액
price = mul_tax_layer.forward(apple_orange_price, tax) # 전체금액 X tax

# 역전파
dprice = 1
dapple_orange_price, dtax_price = mul_tax_layer.backward(dprice)
dapple_price, dorange_price = add_apple_orange_layer.backward(dapple_orange_price)
dorange, dorange_num = mul_orange_layer.backward(dorange_price)
dapple, dapple_num = mul_orange_layer.backward(dapple_price)

print('price all : ', price)
print('dapple_orange_price : ' + str(dapple_orange_price) +
      '\ndtax_price : ' + str(dtax_price) + '\ndapple_price : ' +  str(dapple_price) +
      '\ndorange_price : ' + str(dorange_price) + '\ndorange : ' + str(dorange) +
      '\ndorange_num : ' + str(dorange_num) + '\ndapple : ' + str(dapple) + '\ndapple_num : ' + str(dapple_num))