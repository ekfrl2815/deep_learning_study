import numpy as np
class Relu:
    def __init__(self):
        self.mask = None
    def forward(self, x):
        self.mask = (x <=0 )
        out = x.copy()
        out[self.mask] = 0
        return out
    def backward(self, dout):
        dout[self.mask] = 0
        dx = dout
        return dx

class Sigmoid:
    def __init__(self):
        self.out = None

    def forward(self, x):
        out = 1/(1+np.exp(-x))
        self.out = out
        return out

    def backward(self, dout):
        dx = dout * (1.0 - self.out) * self.out
        return dx

class Affine:
    def __init__(self, w, b):
        self.w = w
        self.b = b
        self.x = None
        self.dw = None
        self.db = None
    def forward(self, x):
        self.x = x
        out = np.dot(x, self.w) + self.b
        return out
    def backward(self, dout):
        dx = np.dot(dout, self.w.T)
        self.dw = np.dot(self.x.T, dout)
        self.db = np.sum(dout, axis=0)
        return dx

relu = Relu()
sig = Sigmoid()
aff = Affine(np.array([[1,1],[1,1]]), np.array([1,1]))
print(aff.forward(np.array([[1,1],[2,2]])))
print(relu.forward(np.array([-1,1,4,3,-3])))
print(relu.backward(np.array([-1,1,4,3,-3])))

