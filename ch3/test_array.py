import numpy as np
def sigmoid(x):
    return 1/(1+np.exp(-x))

def identify_function(x):
    return x

# a = np.array([[1,2,3,4],[2,3,4,5],[3,4,5,6]])
# print(a)
#
# print(np.ndim(a))
# print(a.shape)


# A = np.array([[1,2],[3,4]])
# print(A.shape)
# B = np.array([[5,6],[7,8]])
# print(B.shape)
# print(np.dot(A,B))
#
#
# C = np.array([[[1,2],[3,4]],[[5,6],[7,8]], [[9,10],[11,12]],[[13,14],[15,16]]])
# print(C)

# A = np.array([[1,2],[3,4],[5,6]])
# print(A.shape)
# B = np.array([7,8])
# print(B.shape)
# print(np.dot(A,B))

# X = np.array([1,2])
# print(X.shape)
# W = np.array([[1,3],[2,4]])
# print(W.shape)
# print(W)
# print(np.dot(X,W))
# print(np.dot([2j, 3j], [2j, 3j]))

X = np.array([1.0, 0.5])
W1 = np.array([[0.1, 0.3, 0.5],[0.2, 0.4, 0.6]])
B1 = np.array([0.1, 0.2, 0.3])

print(W1.shape)
print(X.shape)
print(B1.shape)

A1 = np.dot(X,W1) + B1
print(A1)
Z1 = sigmoid(A1)
print(Z1)

W2 = np.array([[0.1, 0.4],[0.2, 0.5],[0.3, 0.6]])
B2 = np.array([0.1, 0.2])
print(Z1.shape)
print(W2.shape)
print(B2.shape)

A2 = np.dot(Z1, W2) + B2
Z2 = sigmoid(A2)
print(A2)
print(Z2)

W3 = np.array([[0.1, 0.3],[0.2, 0.4]])
B3 = np.array([0.1, 0.2])
A3 = np.dot(Z2, W3) + B3
Y = identify_function(A3)
print(Y)