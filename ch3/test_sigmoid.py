import numpy as np
import matplotlib.pylab as plt

def sigmoid(x):
    return 1/(1+np.exp(-x))

def relu(x):
    return np.maximum(0,x)

x = np.arange(-5.0, 5.0, 0.1)
y1 = sigmoid(x)
y2 = relu(x)
plt.plot(x,y1, label="sigmoid")
plt.plot(x,y2, label="ReLU", linestyle="--")
plt.xlabel("x")
plt.ylabel("y")
plt.title("sigmoid & ReLU")
plt.legend()
plt.ylim(-0.1, 1.1)
plt.show()

