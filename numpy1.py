import numpy as np
# x = np.array([1.0, 2.0, 3.0])
# print(x)
# type(x)

# x = np.array([1.0, 2.0, 3.0])
# y = np.array([2.0, 4.0, 6.0])
# print(x+y)
# print(x-y)
# print(x*y)
# print(x/y)

# A = np.array([[1, 2], [3, 4]])
# print(A)
# A.shape
# A.dtype

x = np.array([[51,55], [14,19],[0,4]])
print(x)
print(x[0])
print(x[0][1])

for row in x:
    print(row)

x1 = x.flatten()
print(x1)
print(x1[np.array([0,2,4])])
print(x1>5)
print(x1[x1>15])