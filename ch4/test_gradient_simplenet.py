import sys, os
sys.path.append(os.pardir)
import numpy as np

class simpleNet:
    def __init__(self):
        # self.W = np.random.randn(2,3)
        self.W = np.array([[0.47355232, 0.9977393, 0.84668094],[0.85557411, 0.03563661, 0.69422093]])

    def predict(self, x):
        return np.dot(x, self.W)

    def loss(self, x, t): # 손실함수 : 손실함수가 작을수록 정확도가 높음
        z = self.predict(x)
        y = softmax(z) # 확률
        loss = cross_entropy_error(y, t)
        return loss

def softmax(x):
    c = np.max(x)
    exp_a = np.exp(x - c)
    sum_exp_a = np.sum(exp_a)
    y = exp_a / sum_exp_a
    return y

def cross_entropy_error(y, t):
    if y.ndim == 1 :
        t = t.reshape(1, t.size)
        y = y.reshape(1, y.size)
    batch_size = y.shape[0]
    delta = 1e-7
    return -np.sum(np.log(y[np.arange(batch_size), t] + delta)) / batch_size

def numerical_gradient(f, x):
    h = 1e-4
    grad = np.zeros_like(x) #x와 형상이 같은 배열 생성

    for idx1 in range(x.shape[0]):
        for idx2 in range(x.shape[1]):
            tmp_val = x[idx1][idx2]
            x[idx1][idx2] = tmp_val + h
            fxh1 = f(x)

            x[idx1][idx2] = tmp_val - h
            fxh2 = f(x)

            grad[idx1][idx2] = (fxh1 - fxh2) / (2*h)
            x[idx1][idx2] = tmp_val

    return grad

def gradient_descent(f, init_x, lr=0.01, step_num=100):
    x = init_x
    for i in range(step_num):
        grad = numerical_gradient(f, x)
        x -= lr * grad
    return x

def f(W):
    return net.loss(x, t)

net = simpleNet()
print(net.W)
x = np.array([0.6, 0.9])
p = net.predict(x)
print('predict : ', p)
print(np.argmax(p))

# t = np.array([0,0,1])
t = np.array(2)
print(net.loss(x, t))

print('size : ', net.W.shape[0])
dW = numerical_gradient(f, net.W) #손실함수를 가중치로 미분한다
print(dW)

W = gradient_descent(f, net.W, 0.01, 100) #손실함수의 미분값이 양수이면 W(가중치)를 감소시키고, 손실함수의 미분값이 음수이면 W(가중치)를 증가시킨다.
print(W)