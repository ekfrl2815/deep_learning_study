import numpy as np

# 원-핫 인코딩인 경우는
# 정답 t는 0 혹은 1로 이루어진 리스트로 나오고(예 : [0,1,0,0,0])
# y는 정답에 대한 확률을 나타내며(예 : [0.1, 0.5, 0.2, 0.2, 0.0])
# t*np.log(y)의 합의 음수가 교차 엔트로피가 된다
# 그런데 0은 곱해봤자 0이기 때문에 결국 1인 값과 동일한 순번의 y값들(정답확률)에 log를 씌운 다음 전체를 합한 후 음수 처리한 것이며
# 이것이 교차 엔트로피다.
#
# 원-핫 인코딩이 아닌 경우는
# 정답인 t가 숫자로 나오기 때문에 y[np.arange(batch_size), t]는 y값 중 정답 확률과 동일하다
# 때문에 y[np.arange(batch_size), t])에 log를 씌운다음 전체를 합한 후 음수처리하면 원-핫 인코딩의 교차 엔트로피와 동일하다.

# one-hot encoding인 경우
def cross_entropy_error(y, t) :
    if y.ndim == 1 :
        t = t.reshape(1, t.size)
        y = y.reshape(1, y.size)
    batch_size = y.shape[0]
    delta = 1e-7
    return -np.sum(t*np.log(y + delta)) / batch_size

# one-hot encoding이 아닌 경우
def cross_entropy_error2(y, t):
    if y.ndim == 1 :
        t = t.reshape(1, t.size)
        y = y.reshape(1, y.size)
    batch_size = y.shape[0]
    delta = 1e-7
    return -np.sum(np.log(y[np.arange(batch_size), t] + delta)) / batch_size


y_list = [0.1, 0.2, 0.4, 0.3, 0.0] # 결과 레이블(확률)
t1_list = [0, 0, 1, 0, 0] # 정답 레이블(원 핫 인코딩일 경우는 [0,0,1,0,0...], 원 핫 인코딩이 아닐 경우는 0, 1, 2....]
t2_list = 2 # 정답 레이블(원 핫 인코딩일 경우는 [0,0,1,0,0...], 원 핫 인코딩이 아닐 경우는 0, 1, 2....]

# y_list = [[0.1, 0.2, 0.4, 0.3, 0.0],[0.0, 0.1, 0.2, 0.3, 0.4]] # 결과 레이블(확률)
# t1_list = [[0, 0, 1, 0, 0], [0, 0, 0, 1, 0]] # 정답 레이블(원 핫 인코딩일 경우는 [0,0,1,0,0...], 원 핫 인코딩이 아닐 경우는 0, 1, 2....]
# t2_list = [2, 3] # 정답 레이블(원 핫 인코딩일 경우는 [0,0,1,0,0...], 원 핫 인코딩이 아닐 경우는 0, 1, 2....]


y1 = np.array(y_list)
t1 = np.array(t1_list)
t2 = np.array(t2_list)
print('y의 차원            : ', y1.ndim)
print('y의 shape           : ', y1.shape)
y2 = y1.reshape(1, y1.size)
print('변형 후 y의 차원    : ', y2.ndim)
print('변형 전 y의 shape[0]: ', y1.shape[0])
print('변형 후 y의 shape[0]: ', y2.shape[0])
e = cross_entropy_error(y1, t1)
e2 = cross_entropy_error2(y1, t2)
print('e1 : ', e)
print('e2 : ', e2)


y3 = [[1,2,3,4,5]]
y4 = [[1,2,3,4,5]]
print(np.array(y3)*np.array(y4))