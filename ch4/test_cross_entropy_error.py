import numpy as np

def cross_entropy_error(y, t):
    delta = 1e-7
    return -np.sum(t * np.log(y + delta))


t = [0,0,1,0,0,0,0,0,0,0]
y = [0.1, 0.05, 0.6, 0.0, 0.05, 0.1, 0.0, 0.1, 0.0, 0.0]
print(cross_entropy_error(np.array(y), np.array(t)))
print(np.array(t).ndim)

t1 = np.array(t)
print(t1)
print(t1.ndim)
t2 = t1.reshape(1,t1.size)
print(t2)
print(t2.ndim)
batch_size1 = t1.shape
batch_size2 = t2.shape
print(batch_size1)
print(batch_size2)