import numpy as np
import sys, os
sys.path.append(os.pardir)
from mnist import load_mnist
# from common.functions import *
# from common.gradient import numerical_gradient
def sigmoid(x):
    return 1/(1+np.exp(-x))

def softmax(x):
    c = np.max(x)
    exp_a = np.exp(x - c)
    sum_exp_a = np.sum(exp_a)
    y = exp_a / sum_exp_a
    return y

def cross_entropy_error(y, t):
    if y.ndim == 1 :
        t = t.reshape(1, t.size)
        y = y.reshape(1, y.size)
    batch_size = y.shape[0]
    delta = 1e-7
    # return -np.sum(np.log(y[np.arange(batch_size), t] + delta)) / batch_size
    return -np.sum(t * np.log(y + delta)) / batch_size

def numerical_gradient(f, x):
        h = 1e-4
        grad = np.zeros_like(x)  # x와 형상이 같은 배열 생성
        if x.ndim == 1:
            for idx1 in range(x.shape[0]):
                tmp_val = x[idx1]
                x[idx1] = tmp_val + h
                fxh1 = f(x)

                x[idx1] = tmp_val - h
                fxh2 = f(x)

                grad[idx1] = (fxh1 - fxh2) / (2 * h)
                x[idx1] = tmp_val
        elif x.ndim == 2:
            for idx1 in range(x.shape[0]):
                for idx2 in range(x.shape[1]):
                    tmp_val = x[idx1][idx2]
                    x[idx1][idx2] = tmp_val + h
                    fxh1 = f(x)

                    x[idx1][idx2] = tmp_val - h
                    fxh2 = f(x)

                    grad[idx1][idx2] = (fxh1 - fxh2) / (2 * h)
                    x[idx1][idx2] = tmp_val
        return grad

class TwoLayerNet:
    def __init__(self, input_size, hidden_size, output_size, weight_init_std=0.01):
        self.params = {}
        self.params['W1'] = weight_init_std * np.random.randn(input_size, hidden_size) #input_size X hidden_size 행렬 생성. 기대값 0, 표준편차 1
        self.params['b1'] = np.zeros(hidden_size)
        self.params['W2'] = weight_init_std * np.random.randn(hidden_size, output_size)
        self.params['b2'] = np.zeros(output_size)

    def predict(self, x):
        W1, W2 = self.params['W1'], self.params['W2']
        b1, b2 = self.params['b1'], self.params['b2']

        a1 = np.dot(x, W1) + b1
        z1 = sigmoid(a1)
        a2 = np.dot(z1, W2) + b2
        y = softmax(a2)
        return y

    def loss(self, x, t): # 발생확률(softmax)과 정답으로 손실함수를 구함
        y = self.predict(x)
        return cross_entropy_error(y, t)

    def accuracy(self, x, t):
        y = self.predict(x)
        y = np.argmax(y, axis=1)
        t = np.argmax(t, axis=1)

        accuracy = np.sum(y == t ) / float(x.shape[0])
        return accuracy

    def numerical_gradient(self, x, t):
        loss_W = lambda W : self.loss(x, t)

        grads = {}
        grads['W1'] = numerical_gradient(loss_W, self.params['W1'])
        grads['b1'] = numerical_gradient(loss_W, self.params['b1'])
        grads['W2'] = numerical_gradient(loss_W, self.params['W2'])
        grads['b2'] = numerical_gradient(loss_W, self.params['b2'])

        return grads

(x_train, t_train), (x_test, t_test) = load_mnist(normalize=True, one_hot_label=True)
print('x_train shape : ', x_train.shape)
print('t_train shape : ', t_train.shape)

network = TwoLayerNet(input_size=784, hidden_size=50, output_size=10)
iters_num = 10000 # 반복횟수
train_size = x_train.shape[0]
batch_size = 100 # 미니배치 크기
learning_rate = 0.01

train_loss_list = []
train_acc_list = []
test_acc_list = []

# 1에폭당 반복 수
iter_per_epoch = max(train_size / batch_size, 1)
print('train_size : ', train_size) # 60000
print('batch_size : ', batch_size) #100
print('iter_per_epoch : ', iter_per_epoch) # 600

for i in range(iters_num): # 10000번 반복
    #미니배치 획득
    batch_mask = np.random.choice(train_size, batch_size) # 60000개의 숫자 중 100개를 무작위로 추출하여 리스트로 만듬

    x_batch = x_train[batch_mask] # x_train = 60000 X 784 행렬, x_batch =  100X784 행렬
    t_batch = t_train[batch_mask] # t_train = 60000 X 10 행렬, t_batch = 100X10 행렬

    #기울기 구하기
    grad = network.numerical_gradient(x_batch, t_batch)

    #매개변수 갱신(기울기가 +이면 -방향으로, -이면 +방향으로 가중치와 편향을 변경함)
    for key in ('W1','b1','W2','b2'):
        network.params[key] -= learning_rate * grad[key]

    #학습 경과 기록(가중치와 편향 변경 후 손실함수 다시 확인)
    loss = network.loss(x_batch, t_batch)
    train_loss_list.append(loss)

    # 1에폭당 정확도 계산
    train_acc = network.accuracy(x_train, t_train)
    test_acc = network.accuracy(x_test, t_test)
    print('train acc, test acc : ' + str(train_acc) + "," + str(test_acc))

    # if i % iter_per_epoch == 0 :
    #     train_acc = network.accuracy(x_train, t_train)
    #     test_acc = network.accuracy(x_test, t_test)
    #     train_acc_list.append(train_acc)
    #     test_acc_list.append(test_acc)
    #     print('train acc, test acc : ' + str(train_acc_list) + "," + str(test_acc_list))


print('End')